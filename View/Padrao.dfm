object frmPadrao: TfrmPadrao
  Left = 0
  Top = 0
  BorderStyle = bsSingle
  Caption = 'frmPadrao'
  ClientHeight = 565
  ClientWidth = 1009
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object pnlTopo: TPanel
    Left = 0
    Top = 0
    Width = 1009
    Height = 41
    Align = alTop
    BevelOuter = bvNone
    Color = clHotLight
    ParentBackground = False
    ShowCaption = False
    TabOrder = 0
    object imgTela: TImage
      Left = 904
      Top = 0
      Width = 105
      Height = 41
      Align = alRight
      ExplicitLeft = 496
      ExplicitTop = 8
      ExplicitHeight = 105
    end
    object pnlLogotipo: TPanel
      Left = 0
      Top = 0
      Width = 200
      Height = 41
      Align = alLeft
      BevelOuter = bvNone
      Caption = 'pnlLogotipo'
      Color = clHighlight
      ParentBackground = False
      ShowCaption = False
      TabOrder = 0
      object Label2: TLabel
        Left = 23
        Top = 9
        Width = 48
        Height = 25
        Caption = 'Titulo'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindow
        Font.Height = -19
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
      end
    end
  end
  object pnlMain: TPanel
    Left = 0
    Top = 41
    Width = 1009
    Height = 524
    Align = alClient
    BevelOuter = bvNone
    Caption = 'pnlMain'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Segoe UI'
    Font.Style = []
    ParentFont = False
    ShowCaption = False
    TabOrder = 1
    object pgcMain: TPageControl
      Left = 0
      Top = 0
      Width = 1009
      Height = 524
      ActivePage = tsGrid
      Align = alClient
      Style = tsFlatButtons
      TabOrder = 0
      object tsGrid: TTabSheet
        Caption = 'tsGrid'
        object pnlGrid: TPanel
          Left = 0
          Top = 41
          Width = 1001
          Height = 448
          Align = alClient
          BevelOuter = bvNone
          Caption = 'pnlGrid'
          Padding.Left = 15
          Padding.Top = 15
          Padding.Right = 15
          Padding.Bottom = 25
          ShowCaption = False
          TabOrder = 0
          object DBGrid1: TDBGrid
            Left = 15
            Top = 15
            Width = 971
            Height = 408
            Align = alClient
            DataSource = dsGrid
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -13
            TitleFont.Name = 'Segoe UI'
            TitleFont.Style = []
          end
        end
        object pnlBotaoGrid: TPanel
          Left = 0
          Top = 0
          Width = 1001
          Height = 41
          Align = alTop
          BevelOuter = bvNone
          Caption = 'pnlBotaoGrid'
          Padding.Left = 15
          Padding.Right = 15
          ShowCaption = False
          TabOrder = 1
          object btnEditar: TSpeedButton
            Left = 128
            Top = 0
            Width = 113
            Height = 41
            Align = alLeft
            Caption = 'Editar'
            OnClick = btnEditarClick
            ExplicitLeft = 8
          end
          object btnSair: TSpeedButton
            Left = 354
            Top = 0
            Width = 113
            Height = 41
            Align = alLeft
            Caption = 'Sair'
            OnClick = btnSairClick
            ExplicitLeft = 121
          end
          object btnNovo: TSpeedButton
            Left = 15
            Top = 0
            Width = 113
            Height = 41
            Align = alLeft
            Caption = 'Novo'
            OnClick = btnNovoClick
            ExplicitLeft = -6
            ExplicitTop = -6
          end
          object imgBuscar: TImage
            Left = 925
            Top = 0
            Width = 61
            Height = 41
            Cursor = crHandPoint
            Align = alRight
            Center = True
            Picture.Data = {
              0B546478504E47496D61676589504E470D0A1A0A0000000D4948445200000030
              0000003008060000005702F9870000000473424954080808087C086488000000
              097048597300000B1300000B1301009A9C18000004E2494441546881ED984F6C
              146518C69FEF9D99D6D258F1CFB60650A1311A624A0AA4C49098A8502474BBBB
              B3503CA02763134D249CACD18307634C0C897AD188C60817636665A894A4B612
              F04054103934924D548C1C346E25C102D6DD65DFD703EE3A3BBBB33B3BB3DBF6
              C073DBF7FDBEE77B7EFBCD7FE0A61657CACFA0EDDBB7B7B7B7B747896807336F
              24A255CCDC4E4473007E06700AC061DBB6CFB4346D15D504181D1D353299CC0B
              22324644DD3EFC4E33F3D8F8F8F8C9E6C4AB2F4F806432D95B28142C22DAD0A8
              A988BCDBDDDDBDEFC08103F970F1EAAB2A403C1EEF03F0658D7F3D0BE00A33DF
              4144E43166AAABAB2B7EF0E0C17F9A11D44B158BC762B11500BEA812FE17007B
              99798D6DDBB7D8B61DB97CF972BB886C1691B79979DE357EDBDCDCDCA156052F
              AA62071289C4B4526AABABFC46241279B5D621619AE62A009F02D8EC6A3D6BDB
              F687E1A356571980699A2680C3CE9A883C7FE4C891F7FC988D8C8CB4E5F3794B
              291573942FCDCFCFF74E4E4ECE858F5BA9B24388995F74F53FF01B1E002CCBCA
              1986B187992F38CA777674743C1326642D9500868787EF27A2871DBD2BF97CFE
              E5460D2DCBBA4A442FB9CA7B8206ACA71280AEEB5B9C0D11494D4C4CFC19C454
              D7F5C3CC9C7194360C0D0DDD1E30634D950098B9CFD9504A4D0535B52CAB4044
              5F39ED0CC37828A85F2D9500DC974D11F9358CB1885C7095FCDCC91B56094044
              DC97D44248EFB0F37DC97915BAE4EAAD0A632C22F7952D4414E87CAAA7128052
              EABC2BC0E3618C95528FB8FCD261FCBCE43C894FB87ABB0707073B83982693C9
              AD4AA97B1DA5F3B66D673C27845009607C7C7C46447E283588229D9D9D638D1A
              8E8E8E1ACCFCA6B326229F848BE9ADB23BB188EC77FD7EC534CDA1460C676767
              DF514AAD7794AEE9BAFE7E888C355506D0DFDF7F08C0D95293889839954C269F
              AE67148D469799A6F93180E79C7566360A85C2A626E5AD50C5D3682C167B50D3
              B4D300BA5CADE300F6472291E3CEA7D268347A9761184F021803708FC73A5900
              3B6DDB3ED6A4DC25557DA14926938F89C8048065EE1E33CF13D14FCC7C8D8822
              007ABD7C5C6A0984E7C2A6690E00F80CDEFF6A10351D42F36AA4D3E9DFFAFAFA
              3E62E60E00EB01E8F5CC44E4A288FCAD94BAD563880E60E7DAB56BCFA5D3E91F
              83452E97AFCF2AC3C3C33D9AA6ED514AED00B011C07200606626A28BCC7C0A80
              DDD3D3F379269359A3943A0160450DCBA6ED842F00B70607073BDBDADADA56AE
              5C79B5DA6B66229178C027C4A3B66D7F132443518100FCA81E04331FCB66B33B
              272727B361D6691900E00DE10E6F9AE66BB871FFF85AD7F5A72CCBFACBEF1A2D
              05002A21DCE11389C45B4AA97D8E29DFEABAFE845F88960300FF4330F3B93AE1
              8B3AADEBFA363F100B020000F1787C75369BFDDD47F8A27C412C1880533EC217
              5517C2EBBB66CB9448245EF7191E0036E572B9E9919191DBBC062C388088EC6D
              643C110DD48258700022FA2EC09C815C2E371D8FC79757F49A13CBBF344DDB0D
              60A6D17944340060CA0DB1E000A9546A56D3B42D080E31E1AC2DCA55080076ED
              DA1529140AC701F4D51DECD2F5EBD7EF3E7AF4E81FC022EC405141778299CF14
              C3038B0800048298310CA3EC23C3A21D424EF93C9C66344DDB924AA5669DC525
              0100D485A81A1E58420080278467786091CF01B78AE704337FFF5FE96CADF0C0
              1203006E40F4F7F70F28A556AF5BB76E53ADF037B514F42F628F018F8BE77503
              0000000049454E44AE426082}
            Proportional = True
            ExplicitLeft = 918
            ExplicitTop = -6
          end
          object btnExcluir: TSpeedButton
            Left = 241
            Top = 0
            Width = 113
            Height = 41
            Align = alLeft
            Caption = 'Excluir'
            ExplicitLeft = 121
          end
          object edtBuscar: TEdit
            AlignWithMargins = True
            Left = 634
            Top = 3
            Width = 288
            Height = 35
            Align = alRight
            Alignment = taCenter
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -16
            Font.Name = 'Segoe UI'
            Font.Style = []
            ParentFont = False
            TabOrder = 0
            TextHint = 'Pesquise aqui'
            ExplicitHeight = 29
          end
        end
      end
      object tsEdit: TTabSheet
        Caption = 'tsEdit'
        ImageIndex = 1
        object pnlBotaoEdit: TPanel
          Left = 0
          Top = 0
          Width = 1001
          Height = 41
          Align = alTop
          BevelOuter = bvNone
          Caption = 'pnlBotaoGrid'
          Padding.Left = 15
          Padding.Right = 15
          ShowCaption = False
          TabOrder = 0
          object btnCancelar: TSpeedButton
            Left = 128
            Top = 0
            Width = 113
            Height = 41
            Align = alLeft
            Caption = 'Cancelar'
            OnClick = btnCancelarClick
            ExplicitLeft = 0
          end
          object btnSalvar: TSpeedButton
            Left = 15
            Top = 0
            Width = 113
            Height = 41
            Align = alLeft
            Caption = 'Salvar'
            OnClick = btnSalvarClick
            ExplicitLeft = 8
          end
        end
        object pnlEdit: TPanel
          Left = 0
          Top = 41
          Width = 1001
          Height = 448
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 1
        end
      end
    end
  end
  object dsGrid: TDataSource
    DataSet = Query
    Left = 620
    Top = 206
  end
  object Query: TFDQuery
    Left = 580
    Top = 206
  end
  object Mem: TFDMemTable
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    Left = 540
    Top = 209
  end
end
