unit Model.Produto;

interface

uses
  FireDAC.Comp.Client, Interfaces.Model, DMConexao;

type
  ModelProduto = class(TInterfacedObject, iModelProduto)

  private
    FQuery : TFDQuery;
    DM     : TDataModule1;
    FMem   : TFDMemTable;
    FID: integer;
    FDESCRICAO: string;


    function GetId: Integer;
    procedure SetFID(const Value: integer);
    procedure SetFDESCRICAO(const Value: string);
  public
    constructor Create;
    destructor Destroy; override;

    function ConectarBanco(AValue : TFDQuery) : iModelProduto;
    procedure Novo(Avalue :TFDMemTable); overload;
    procedure Novo; overload;
    procedure Editar(Avalue :TFDMemTable); overload;
    procedure Editar; overload;
    function Salvar(AValue : TFDMemTable): Boolean; overload;
    function Salvar: Boolean; overload;
    procedure Excluir;
    function Consultar(Valor : string ) : TFDQuery;

    property id: integer read FID write SetFID;
    property descricao: string read FDESCRICAO write SetFDESCRICAO;
  end;

implementation

uses
  System.SysUtils;

{ ModelProduto }

function ModelProduto.ConectarBanco(AValue: TFDQuery): iModelProduto;
begin
  AValue.Connection := DM.Conexao;
  FQuery := AValue;
end;

function ModelProduto.Consultar(Valor : string ) : TFDQuery;
begin
  FQuery.SQL.Clear;
  if Valor = '' then
    FQuery.Open('select ID, DESCRICAO, UNIDADE, PRECO_VENDA, PRECO_COMPRA from PRODUTO')
  else
  begin
    FQuery.SQL.Add('Select');
    FQuery.SQL.Add('ID, DESCRICAO, UNIDADE,');
    FQuery.SQL.Add('PRECO_VENDA, PRECO_COMPRA');
    FQuery.SQL.Add('from PRODUTO');
    FQuery.SQL.Add('where 1=1 and (');
    FQuery.SQL.Add('ID like '+QuotedStr(Valor+'%')+' or');
    FQuery.SQL.Add('descricao like '+QuotedStr(Valor+'%')+' or');
    FQuery.SQL.Add('unidade like '+QuotedStr(Valor+'%')+ 'or');
    FQuery.SQL.Add('preco_venda like '+QuotedStr(Valor+'%')+' or');
    FQuery.SQL.Add('preco_compra like '+QuotedStr(Valor+'%')+')');
    FQuery.Open;
  end;
  Result := FQuery;
end;

constructor ModelProduto.Create;
begin
  FQuery := TFDQuery.Create(nil);
  FMem := TFDMemTable.Create(nil);
  DM := TDataModule1.Create(nil);
end;

destructor ModelProduto.Destroy;
begin
  inherited;
end;

procedure ModelProduto.Editar(AVAlue : TFDMemTable);
begin
  AValue.Edit;
end;

procedure ModelProduto.Editar;
begin
  FQuery.Edit;
end;

procedure ModelProduto.Excluir;
begin
  FQuery.Delete;
end;

procedure ModelProduto.Novo(AVAlue : TFDMemTable);
begin
  AValue.Append;

  FQuery.FieldByName('id').AsInteger            := GetId;
end;

function ModelProduto.Salvar(AValue : TFDMemTable): Boolean;
begin
  Result := True;
  try
    FQuery.FieldByName('descricao').AsString      := AValue.FieldByName('descricao').AsString;
    FQuery.FieldByName('unidade').AsString        := AValue.FieldByName('unidade').AsString;
    FQuery.FieldByName('preco_venda').AsCurrency  := AValue.FieldByName('preco_venda').AsCurrency;
    FQuery.FieldByName('preco_compra').AsCurrency := AValue.FieldByName('preco_compra').AsCurrency;
    FQuery.Post;
    AValue.Post;
  except on E: Exception do
    Result := False;
  end;

end;

function ModelProduto.Salvar: Boolean;
begin
  Result := True;
  try
    FQuery.Post;
  except on E: Exception do
    Result := False;
  end;
end;

procedure ModelProduto.SetFDESCRICAO(const Value: string);
begin
  FDESCRICAO := Value;
end;

procedure ModelProduto.SetFID(const Value: integer);
begin
  FID := Value;
end;

function ModelProduto.GetId: Integer;
var
  VQry: TFDQuery;
begin
  VQry := TFDQuery.Create(nil);
  VQry.Connection := DM.Conexao;
  try
    VQry.Open('select gen_id(gen_produto_id, ' + IntToStr(1) + ' ) from rdb$database');
    try
      Result := VQry.Fields[0].AsInteger;
    finally
      VQry.Close;
    end;
  finally
    VQry.Free;
  end;
end;


procedure ModelProduto.Novo;
begin
  FQuery.Append;
  FQuery.FieldByName('id').AsInteger := GetId;
end;

end.
