inherited frmProduto: TfrmProduto
  Caption = 'Teste de Cadastro usando OO'
  Position = poDesktopCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlTopo: TPanel
    inherited imgTela: TImage
      Left = 946
      Width = 63
      Center = True
      Picture.Data = {
        0B546478504E47496D61676589504E470D0A1A0A0000000D4948445200000030
        0000003008060000005702F987000000097048597300000B1300000B1301009A
        9C180000051E494441546881D59A4D6C545514C77FF7CDF74C0BB4A12D44508C
        261A59A0D06265A331A9AC4A9A681016652112D976216B5C8A89B865810B30D1
        CCC686AE686382C6906287262456E44381C0A2A558684BE7FBBDE3E2CD40A7F3
        DE9BFBA6D34EF86D66E69EFBEEFD9F773FE6DE73AFA2018CA4249E868F803E05
        6F013B800E2051CAB204CC027705FE02C6E230DADFADD2ABAD5BD5FB60F2B2B4
        13E100C200B6F898CF2232C0288A61725C38B84FCDD5A3C3B703C92969910C43
        0ABE045AEBA9D4814505A732214E1FD9A596FC3CA8EDC099948436C151052781
        2EBF0A359916F8EA099CFDA25B15741ED072E0C73F647BC06018D8BD2A79FA4C
        9A160387F7AAFBB532D674203929EF61F1336BF7D6DD98B1140387F6A871AF4C
        869731392183585C62FDC5037419C2A5E4840C7A65726D81E4840CA238D7785D
        FE11C5E0A77BD40F4E3647077EBA2ABD86F02B105E5365FAE42CC5074EDDA9CA
        81D2809D40A3DBC4CC055A0A8FAAD28301452412A8549033299A5295F7696833
        99C0865A5501CC98163D2B077685036752126A837134679B7D0FCFF352FACFAA
        F4ECB66E965EFFB0222D71FB17A20FAE56E5BD19DCC978E720B1B0D68438F918
        7A974FB1158378131CD5156F4891AEEC4D479B19DBE890B6C931EFCB855BDC99
        C9EB5409B0BB5DF8AC4247F94B724A5A4A7F525A74E4EE10B49C2B36A36D5569
        56AC3A0D20AAF26C7CFAAF6EB588E2E4B96B525E633D7740320CE163BADC9AFE
        DBD566C5ABC59A2E0E00BCC90D9E66ABC7870B5B620586CA3F0CB017660A4EE8
        9600C2D6F4756793323023D583D28C6C00E5DCCFDF89DC607ADEF2513B279297
        A51DCA2D10E100D0A25B406BE1112DC5FF1C6D56740318816A8311C072700CA0
        CB98C35A98D5AD1EA055C2F443D9017B49ACCDD68CCBDB07CCA8F36005F7810C
        F06AE13A05535F83C2D66C8CA4248EBD9ED7C6ABFF7B89F4B2BD1DBAC1C305FD
        6E04EC1F4949DC48FBDC8C84AC2C1DD93BAE76B7D9A696ED8DE03DE6E77D6DD0
        6259A1CF00FAFC3CD595B989C2FD4DD5DB020165D199BE05DA9311007DC1D21E
        569BD7AC5BD0BEDDD51E89B7102ACC3BDA8C78ABE7B3EF876FF37B7A176D09CF
        45F23344B13388BD01D7C66AED048F16086FDE066E6F3AF3041EBAFFD514A493
        D905A12DE19A65253B0CECE88136527F1C40ABEC82C382CF830E83E7A18FC610
        8ED767AB8F44B0D12592CF82B900E104580510816018724B1068FCF622881D74
        6A5C2BDCFECD76221806AB68A7194128E620EC37745493A52076C4AC710EE433
        F66731F73CCD322B6D8D63D600EE36BAD475E4AE518A55BE902861CA00C69A2D
        64158C197118C50EB4BE6864A28A31A314E21E6DB69A3AB8D8DFADD2F6A24331
        DC6431BE116CCDB603392E008BCD14E493459567044A0E1CDCA7E604BE69AE26
        7D149C2A1F883C5BB7E6427C0BCC344D953ED39910A7CB3F9E397064975A128D
        B890E51DD05E1545A95DB6124E2E3FC5A958CC3D81B36D700C8FE8DC15DE25F5
        4F86C58CBFAD930E7933C02B5B3C97EB93738AEF9727AC2AB8BBCE4C078AF47C
        DCAB1E2C4FAC6AB3C37BD57D4B3100E456DA9A48CE808195E2C1E584E6D01E35
        8E706CED75E9218ACF3FE956579C6CAEA3E6608F3A8F7004D00E1DAF0139AFD3
        19D038E42B9DD60CF3221EF281DD9D4C8B1E60B261D26A33695AF4D4120F1A0E
        803DB01F43AF128E03D3AB96E7CEB4128E3F865E9D3362A8E3AAC1B96B928815
        18123B1CDFB0AB06085F67C37CB766570D5692BC2CED12A6BF1425DE4F7D973D
        2E0A0CAB3C23EB76D9C3899194C4B3421FD0278A9D785CB751C2143016558C35
        E2BACDFFD83BAFE81C42A1B60000000049454E44AE426082}
      Proportional = True
      ExplicitLeft = 936
      ExplicitTop = 0
      ExplicitWidth = 63
      ExplicitHeight = 41
    end
    inherited pnlLogotipo: TPanel
      Width = 217
      ExplicitWidth = 217
      inherited Label2: TLabel
        Width = 182
        Caption = 'Cadastro -> Produtos'
        ExplicitWidth = 182
      end
    end
  end
  inherited pnlMain: TPanel
    inherited pgcMain: TPageControl
      ActivePage = tsEdit
      inherited tsGrid: TTabSheet
        inherited pnlGrid: TPanel
          inherited DBGrid1: TDBGrid
            Columns = <
              item
                Expanded = False
                FieldName = 'ID'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DESCRICAO'
                Width = 250
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'UNIDADE'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'PRECO_VENDA'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'PRECO_COMPRA'
                Visible = True
              end>
          end
        end
        inherited pnlBotaoGrid: TPanel
          inherited imgBuscar: TImage
            OnClick = imgBuscarClick
          end
          inherited btnExcluir: TSpeedButton
            OnClick = btnExcluirClick
          end
        end
      end
      inherited tsEdit: TTabSheet
        inherited pnlEdit: TPanel
          object Label1: TLabel
            Left = 72
            Top = 24
            Width = 12
            Height = 17
            Caption = 'ID'
          end
          object Label3: TLabel
            Left = 72
            Top = 72
            Width = 57
            Height = 17
            Caption = 'Descri'#231#227'o'
          end
          object Label4: TLabel
            Left = 72
            Top = 120
            Width = 19
            Height = 17
            Caption = 'UN'
          end
          object Label5: TLabel
            Left = 72
            Top = 168
            Width = 64
            Height = 17
            Caption = 'Venda (R$)'
          end
          object Label6: TLabel
            Left = 72
            Top = 216
            Width = 74
            Height = 17
            Caption = 'Compra (R$)'
          end
          object dbtxtID: TDBText
            Left = 72
            Top = 44
            Width = 65
            Height = 17
            DataField = 'ID'
            DataSource = dsGrid
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Segoe UI'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object edtDESCRICAO: TDBEdit
            Left = 72
            Top = 89
            Width = 289
            Height = 25
            DataField = 'DESCRICAO'
            DataSource = dsGrid
            TabOrder = 0
          end
          object edtUnidade: TDBEdit
            Left = 72
            Top = 137
            Width = 65
            Height = 25
            DataField = 'UNIDADE'
            DataSource = dsGrid
            TabOrder = 1
          end
          object edtVenda: TDBEdit
            Left = 72
            Top = 186
            Width = 121
            Height = 25
            DataField = 'PRECO_VENDA'
            DataSource = dsGrid
            TabOrder = 2
          end
          object edtCompra: TDBEdit
            Left = 72
            Top = 234
            Width = 121
            Height = 25
            DataField = 'PRECO_COMPRA'
            DataSource = dsGrid
            TabOrder = 3
          end
        end
      end
    end
  end
  inherited dsGrid: TDataSource
    Left = 916
    Top = 478
  end
  inherited Query: TFDQuery
    Left = 812
    Top = 478
    object QueryID: TIntegerField
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object QueryDESCRICAO: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      Size = 150
    end
    object QueryUNIDADE: TStringField
      DisplayLabel = 'Unidade'
      FieldName = 'UNIDADE'
      Origin = 'UNIDADE'
      Size = 3
    end
    object QueryPRECO_VENDA: TBCDField
      DisplayLabel = 'Pre'#231'o Venda'
      FieldName = 'PRECO_VENDA'
      Origin = 'PRECO_VENDA'
      currency = True
      Precision = 18
      Size = 2
    end
    object QueryPRECO_COMPRA: TBCDField
      DisplayLabel = 'Pre'#231'o Compra'
      FieldName = 'PRECO_COMPRA'
      Origin = 'PRECO_COMPRA'
      currency = True
      Precision = 18
      Size = 2
    end
  end
  inherited Mem: TFDMemTable
    Left = 868
    Top = 481
  end
end
