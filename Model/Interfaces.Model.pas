unit Interfaces.Model;

interface

uses
  FireDAC.Comp.Client;
  type

    iModelProduto = interface
      ['{B589BAA8-6A72-4CC2-B470-AC2C70F60E9B}']
      function ConectarBanco(AValue : TFDQuery) : iModelProduto;
      procedure Novo(Avalue :TFDMemTable); overload;
      procedure Novo; overload;
      procedure Editar(Avalue :TFDMemTable); overload;
      procedure Editar; overload;
      function Salvar(AValue : TFDMemTable): Boolean; overload;
      function Salvar: Boolean; overload;
      procedure Excluir;
      function Consultar(Valor : string ) : TFDQuery;
    end;

implementation

end.
