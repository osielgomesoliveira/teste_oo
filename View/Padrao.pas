unit Padrao;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls, Data.DB,
  Vcl.Buttons, Vcl.Grids, Vcl.DBGrids, Vcl.ComCtrls, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, dxGDIPlusClasses;

type
  TfrmPadrao = class(TForm)
    pnlTopo: TPanel;
    pnlLogotipo: TPanel;
    Label2: TLabel;
    imgTela: TImage;
    pnlMain: TPanel;
    pgcMain: TPageControl;
    tsGrid: TTabSheet;
    tsEdit: TTabSheet;
    pnlGrid: TPanel;
    pnlBotaoGrid: TPanel;
    DBGrid1: TDBGrid;
    pnlBotaoEdit: TPanel;
    btnCancelar: TSpeedButton;
    btnSalvar: TSpeedButton;
    btnEditar: TSpeedButton;
    btnSair: TSpeedButton;
    btnNovo: TSpeedButton;
    edtBuscar: TEdit;
    imgBuscar: TImage;
    dsGrid: TDataSource;
    Query: TFDQuery;
    pnlEdit: TPanel;
    btnExcluir: TSpeedButton;
    Mem: TFDMemTable;
    procedure btnSairClick(Sender: TObject);
    procedure btnNovoClick(Sender: TObject);
    procedure btnEditarClick(Sender: TObject);
    procedure btnSalvarClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmPadrao: TfrmPadrao;

implementation

{$R *.dfm}

procedure TfrmPadrao.btnCancelarClick(Sender: TObject);
begin
  pgcMain.ActivePage := tsGrid;
end;

procedure TfrmPadrao.btnSalvarClick(Sender: TObject);
begin
  pgcMain.ActivePage := tsGrid;
end;

procedure TfrmPadrao.FormShow(Sender: TObject);
begin
  tsGrid.TabVisible  := False;
  tsEdit.TabVisible  := False;
  pgcMain.ActivePage := tsGrid;
end;

procedure TfrmPadrao.btnEditarClick(Sender: TObject);
begin
  pgcMain.ActivePage := tsEdit;
end;

procedure TfrmPadrao.btnSairClick(Sender: TObject);
begin
  Close;
end;

procedure TfrmPadrao.btnNovoClick(Sender: TObject);
begin
  pgcMain.ActivePage := tsEdit;
end;

end.
