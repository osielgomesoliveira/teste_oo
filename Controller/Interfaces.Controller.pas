unit Interfaces.Controller;

interface

uses
  FireDAC.Comp.Client;

type
  iProduto = interface
    ['{FF48A17E-F56C-4AE3-9A46-8F535D3317ED}']
      function ConectarBanco(AValue : TFDQuery) : iProduto;
      procedure Novo(Avalue :TFDMemTable); overload;
      procedure Novo; overload;
      procedure Editar(Avalue :TFDMemTable); overload;
      procedure Editar; overload;
      function Salvar(AValue : TFDMemTable): Boolean; overload;
      function Salvar: Boolean; overload;
      procedure Excluir;
      function Consultar(Valor : string ) : TFDQuery;


  end;

implementation

end.
