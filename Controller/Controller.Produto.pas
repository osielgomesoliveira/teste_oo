unit Controller.Produto;

interface

uses
  Interfaces.Controller, FireDAC.Comp.Client, Interfaces.Model;

type
  TControllerProduto = class(TInterfacedObject, iProduto)
    private
      FModel : iModelProduto;
    public
      constructor Create;

      function ConectarBanco(AValue : TFDQuery) : iProduto;
      procedure Novo(Avalue :TFDMemTable); overload;
      procedure Novo; overload;
      procedure Editar(Avalue :TFDMemTable); overload;
      procedure Editar; overload;
      function Salvar(AValue : TFDMemTable): Boolean; overload;
      function Salvar: Boolean; overload;
      procedure Excluir;
      function Consultar(Valor : string ) : TFDQuery;
  end;


implementation

uses
  Model.Produto;

{ ControllerProduto }

function TControllerProduto.ConectarBanco(AValue: TFDQuery): iProduto;
begin
  Result := Self;
  FModel.ConectarBanco(AVAlue);
end;

function TControllerProduto.Consultar(Valor : string ) : TFDQuery;
begin
  Result := FModel.Consultar(Valor);
end;

constructor TControllerProduto.Create;
begin
 FModel := ModelProduto.Create;
end;

procedure TControllerProduto.Editar(AValue : TFDMemTable);
begin
  FModel.Editar(AValue);
end;

procedure TControllerProduto.Editar;
begin
  FModel.Editar;
end;

procedure TControllerProduto.Excluir;
begin
  FModel.Excluir;
end;

procedure TControllerProduto.Novo;
begin
  FModel.Novo;
end;

procedure TControllerProduto.Novo(Avalue :TFDMemTable);
begin
  FModel.Novo(AValue);
end;

function TControllerProduto.Salvar(AValue : TFDMemTable): Boolean;
begin
  Result := FModel.Salvar(AValue);
end;

function TControllerProduto.Salvar: Boolean;
begin
  Result := FModel.Salvar;
end;

end.
