unit Principal;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.Buttons, Vcl.ExtCtrls, Vcl.StdCtrls,
  dxGDIPlusClasses;

type
  TfrmPrincipal = class(TForm)
    pnlTopo: TPanel;
    pnlMenu: TPanel;
    pnlMain: TPanel;
    pnlLogotipo: TPanel;
    SpeedButton1: TSpeedButton;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label6: TLabel;
    imgWeather: TImage;
    lblHora: TLabel;
    imgLogo: TImage;
    Label8: TLabel;
    Panel1: TPanel;
    Label4: TLabel;
    Label5: TLabel;
    Hora: TTimer;
    procedure SpeedButton1Click(Sender: TObject);
    procedure HoraTimer(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmPrincipal: TfrmPrincipal;

implementation

uses
  Produto;

{$R *.dfm}

procedure TfrmPrincipal.HoraTimer(Sender: TObject);
begin
  lblHora.Caption := FormatDateTime('hh:mm', Now);
end;

procedure TfrmPrincipal.SpeedButton1Click(Sender: TObject);
begin
  frmProduto.Parent := pnlMain;
  frmProduto.Show;
end;

end.
