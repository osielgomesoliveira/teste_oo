unit Produto;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Padrao, Data.DB, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, Vcl.StdCtrls, dxGDIPlusClasses,
  Vcl.Buttons, Vcl.Grids, Vcl.DBGrids, Vcl.ComCtrls, Vcl.ExtCtrls,
  Interfaces.Controller, Controller.Produto, Vcl.DBCtrls, Vcl.Mask;

type
  TfrmProduto = class(TfrmPadrao)
    Label1: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    QueryID: TIntegerField;
    QueryDESCRICAO: TStringField;
    QueryUNIDADE: TStringField;
    QueryPRECO_VENDA: TBCDField;
    QueryPRECO_COMPRA: TBCDField;
    edtDESCRICAO: TDBEdit;
    dbtxtID: TDBText;
    edtUnidade: TDBEdit;
    edtVenda: TDBEdit;
    edtCompra: TDBEdit;
    procedure btnNovoClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure imgBuscarClick(Sender: TObject);
    procedure btnEditarClick(Sender: TObject);
    procedure btnSalvarClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnExcluirClick(Sender: TObject);
  private
    FProduto: iProduto;

    { Private declarations }
  public
    property Produto: iProduto read FProduto write FProduto;
  end;

var
  frmProduto: TfrmProduto;

implementation

uses
  DMConexao;


{$R *.dfm}

procedure TfrmProduto.btnCancelarClick(Sender: TObject);
begin
  inherited;
  Query.Cancel;
end;

procedure TfrmProduto.btnEditarClick(Sender: TObject);
begin
  inherited;
  Produto.Editar;
end;

procedure TfrmProduto.btnExcluirClick(Sender: TObject);
begin
  inherited;
  if MessageBox(Handle, 'Tem certeza que deseja exluir o registro?', 'Mensagem do Sistema', MB_YESNO+MB_ICONQUESTION) = mrYes then
    Produto.Excluir;
end;

procedure TfrmProduto.btnNovoClick(Sender: TObject);
begin
  inherited;
  Produto.Novo;
end;

procedure TfrmProduto.btnSalvarClick(Sender: TObject);
begin
  inherited;

  case Produto.Salvar of
    True :
    begin
      MessageBox(Handle, 'Salvo com sucesso!!!', 'Mensagem do Sistema', MB_OK+MB_ICONINFORMATION);
    end;
    False : MessageBox(Handle, 'Erro ao salvar!!!', 'Mensagem do Sistema', MB_OK+MB_ICONERROR);
  end;
end;

procedure TfrmProduto.FormCreate(Sender: TObject);
begin
  inherited;
  Produto := TControllerProduto.Create;
  Produto.ConectarBanco(Query);

  Produto.Consultar('')
end;

procedure TfrmProduto.imgBuscarClick(Sender: TObject);
begin
  inherited;
  Produto.Consultar(edtBuscar.Text);
end;

end.
