program Teste_OO;

uses
  Vcl.Forms,
  Principal in 'View\Principal.pas' {frmPrincipal},
  Padrao in 'View\Padrao.pas' {frmPadrao},
  Produto in 'View\Produto.pas' {frmProduto},
  DMConexao in 'Model\DMConexao.pas' {DataModule1: TDataModule},
  Controller.Produto in 'Controller\Controller.Produto.pas',
  Model.Produto in 'Model\Model.Produto.pas',
  Interfaces.Controller in 'Controller\Interfaces.Controller.pas',
  Interfaces.Model in 'Model\Interfaces.Model.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TfrmPrincipal, frmPrincipal);
  Application.CreateForm(TfrmPadrao, frmPadrao);
  Application.CreateForm(TfrmProduto, frmProduto);
  Application.CreateForm(TDataModule1, DataModule1);
  Application.Run;
end.
